package parsing

import (
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"

	"github.com/PuerkitoBio/goquery"
)

type Site struct {
	Id                  int
	Url                 string
	NewsItemBlock       string
	NewsItemTitle       string
	NewsItemDescription string
	NewsItemLink        string
	NewsItemDate        string
}

type NewsItem struct {
	Id          int
	Title       string
	Description string
	Link        string
	Date        string
}

type rss struct {
	XmlName xml.Name `xml:"rss"`
	Version string   `xml:"version,attr"`
	Channel channel  `xml:"channel"`
}

type channel struct {
	XmlName xml.Name `xml:"channel"`
	Item    []item   `xml:"item"`
}

type item struct {
	XmlName     xml.Name `xml:"item"`
	Title       string   `xml:"title"`
	Link        string   `xml:"link"`
	Description string   `xml:"description"`
	Date        string   `xml:"pubDate"`
}

type parser struct {
	client http.Client
}

func NewParser(client http.Client) *parser {
	return &parser{client}
}

func (p *parser) Parse(siteItem Site) (news []NewsItem, err error) {
	resp, err := p.client.Get(siteItem.Url)
	if err != nil {
		log.Printf("Request to %s was failed. Error: %s", siteItem.Url, err)

		return
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		log.Printf("Response from %s has an incorrect status. Satus code: %d", siteItem.Url, resp.StatusCode)

		return
	}

	if strings.Contains(resp.Header.Values("content-type")[0], "xml") {
		news, err = parseRss(resp, siteItem)
	} else if strings.Contains(resp.Header.Values("content-type")[0], "html") {
		news, err = parseHtml(resp, siteItem)
	} else {
		log.Printf("Wrong type of document from source url: %s", siteItem.Url)
	}

	return
}

func parseRss(resp *http.Response, siteItem Site) (news []NewsItem, err error) {
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Printf("Can't read request body from url %s.\n err: %s", siteItem.Url, err)

		return
	}

	rss := &rss{}
	err = xml.Unmarshal(body, rss)
	if err != nil {
		log.Printf("Can't unmurshal xml from url %s.\n err: %s", siteItem.Url, err)

		return
	}

	for _, rssNewsItem := range rss.Channel.Item {
		news = append(news, NewsItem{
			Title:       rssNewsItem.Title,
			Description: rssNewsItem.Description,
			Link:        rssNewsItem.Link,
			Date:        rssNewsItem.Date,
		})
	}

	return
}

func parseHtml(resp *http.Response, siteItem Site) (news []NewsItem, err error) {
	doc, err := goquery.NewDocumentFromReader(resp.Body)
	if err != nil {
		log.Printf("Can't make a new document from url: %s.\n err: %s", siteItem.Url, err)
		
		return 
	}

	newsItemBlock := siteItem.NewsItemBlock
	newsTitleSelector := siteItem.NewsItemTitle
	descriptionSelector := siteItem.NewsItemDescription
	dateSelector := siteItem.NewsItemDate
	linkSelector := siteItem.NewsItemLink

	doc.Find(newsItemBlock).Each(func(i int, s *goquery.Selection) {
		item := NewsItem{
			Title:       s.Find(newsTitleSelector).Text(),
			Description: s.Find(descriptionSelector).Text(),
			Date:        s.Find(dateSelector).Text(),
		}
		if link, ok := s.Find(linkSelector).Attr("href"); ok {
			item.Link = link
		}

		news = append(news, item)
	})

	if len(news) == 0 {
		err = fmt.Errorf("Incorrect selectors from source url: %s. Nothing was found", siteItem.Url)
	}

	return
}
