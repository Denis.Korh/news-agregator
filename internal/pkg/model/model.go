package model

import (
	"database/sql"
	"fmt"
	"log"
	"news-agregator/internal/pkg/parsing"

	_ "github.com/lib/pq"
)

type model struct {
	conn *sql.DB
}

func NewModel(conn *sql.DB) *model {
	return &model{conn}
}

func (m *model) GetSites() (siteItems []parsing.Site, err error) {
	rows, err := m.conn.Query("SELECT * FROM sites")
	if err != nil {
		return
	}
	defer rows.Close()

	for rows.Next() {
		site := parsing.Site{}
		err = rows.Scan(&site.Id, &site.Url, &site.NewsItemBlock, &site.NewsItemTitle, &site.NewsItemDescription, &site.NewsItemLink, &site.NewsItemDate)
		if err != nil {
			return
		}
		siteItems = append(siteItems, site)
	}

	return
}

func (m *model) DeleteAllSites() (err error) {
	_, err = m.conn.Exec("TRUNCATE news_items")

	return
}

func (m *model) AddNewsItem(news_item parsing.NewsItem) (err error) {
	_, err = m.conn.Exec("INSERT INTO news_items (title, description, link, date) VALUES($1, $2, $3, $4)", news_item.Title, news_item.Description, news_item.Link, news_item.Date)

	return
}

func (m *model) AddSite(site parsing.Site) (err error) {
	_, err = m.conn.Exec("INSERT INTO sites (url, news_item_path, title_path, description_path, link_path, date_path) VALUES($1, $2, $3, $4, $5, $6)", site.Url, site.NewsItemBlock, site.NewsItemTitle, site.NewsItemDescription, site.NewsItemLink, site.NewsItemDate)

	return
}

func (m *model) DeleteSite(siteId string) (err error) {
	_, err = m.conn.Exec("DELETE FROM sites WHERE id=$1", siteId)

	return
}

func (m *model) NewsSearch(searchString string) (allNews []parsing.NewsItem, err error) {
	rows, err := m.conn.Query(fmt.Sprintf("SELECT * FROM news_items WHERE strpos(title, '%s')>0", searchString))
	if err != nil {
		return
	}
	defer rows.Close()

	for rows.Next() {
		news_item := parsing.NewsItem{}
		err = rows.Scan(&news_item.Id, &news_item.Title, &news_item.Description, &news_item.Link, &news_item.Date)
		if err != nil {
			log.Fatal(err)
		}
		allNews = append(allNews, news_item)
	}

	return
}
