CREATE TABLE IF NOT EXISTS sites (
  id INT GENERATED ALWAYS AS IDENTITY,
  url VARCHAR NOT NULL,
  news_item_path VARCHAR,
  title_path VARCHAR,
  description_path VARCHAR,
  link_path VARCHAR,
  date_path VARCHAR
);

CREATE TABLE IF NOT EXISTS news_items (
    id INT GENERATED ALWAYS AS IDENTITY,
    title VARCHAR NOT NULL,
    description VARCHAR,
    link VARCHAR,
    date VARCHAR
);
