package news_agregator

import (
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"news-agregator/internal/pkg/parsing"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

type mockedModel struct {
	mock.Mock
}

func (mm *mockedModel) GetSites() (siteItems []parsing.Site, err error) {
	args := mm.Called()

	return args.Get(0).([]parsing.Site), args.Error(1)
}

func (mm *mockedModel) DeleteAllSites() (err error) {

	return err
}

func (mm *mockedModel) AddNewsItem(news_item parsing.NewsItem) (err error) {
	args := mm.Called(news_item)

	return args.Error(0)
}

func (mm *mockedModel) AddSite(site parsing.Site) (err error) {
	args := mm.Called(site)

	return args.Error(0)
}

func (mm *mockedModel) DeleteSite(siteId string) (err error) {
	args := mm.Called(siteId)

	return args.Error(0)
}

func (mm *mockedModel) NewsSearch(searchString string) (allNews []parsing.NewsItem, err error) {
	args := mm.Called(searchString)

	return args.Get(0).([]parsing.NewsItem), args.Error(1)
}

type mockedParser struct {
	mock.Mock
}

func (mp *mockedParser) Parse(siteItem parsing.Site) (news []parsing.NewsItem, err error) {
	args := mp.Called(siteItem)

	return args.Get(0).([]parsing.NewsItem), args.Error(1)
}

type mockedTemplates struct {
	mock.Mock
}

func (mt *mockedTemplates) ExecuteTemplate(wr io.Writer, name string, data interface{}) error {
	args := mt.Called(wr, name, data)
	_, _ = fmt.Fprint(wr, data)

	return args.Error(0)
}

func getTestApplication() *news_agregator {
	return &news_agregator{
		db:        new(mockedModel),
		parser:    new(mockedParser),
		templates: new(mockedTemplates),
	}
}

func TestIndexHandler(t *testing.T) {
	assert := assert.New(t)
	app := getTestApplication()

	siteSample := []parsing.Site{
		parsing.Site{
			Id:  1,
			Url: "https://test.ru",
		},
	}

	newsItemSamples := []parsing.NewsItem{
		parsing.NewsItem{
			Id:          1,
			Title:       "Заголовок 1",
			Description: "Описание 1",
			Link:        "https://link1.ru",
		},
		parsing.NewsItem{
			Id:          1,
			Title:       "Заголовок 2",
			Description: "Описание 2",
			Link:        "https://link2.ru",
		},
	}

	app.db.(*mockedModel).On("GetSites").Return(siteSample, nil)
	app.parser.(*mockedParser).On("Parse", siteSample[0]).Return(newsItemSamples, nil)
	app.db.(*mockedModel).On("DeleteAllSites").Return(nil)
	for i := range newsItemSamples {
		app.db.(*mockedModel).On("AddNewsItem", newsItemSamples[i]).Return(nil)
	}

	req, _ := http.NewRequest("GET", "/", nil)
	rr := httptest.NewRecorder()

	app.templates.(*mockedTemplates).On("ExecuteTemplate", rr, "index", newsItemSamples).Return(nil)

	handler := http.HandlerFunc(app.index)
	handler.ServeHTTP(rr, req)
	assert.Equal(http.StatusOK, rr.Code)
	assert.Contains(rr.Body.String(), "Заголовок 1")
	assert.Contains(rr.Body.String(), "Заголовок 2")
}
