package parsing

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

var testParser = parser{http.Client{}}

func TestHttpRequestErr(t *testing.T) {
	incorrectUrlSite := Site{Url: "wrong-response-status.ru/"}
	_, err := testParser.Parse(incorrectUrlSite)
	if err == nil {
		t.Errorf("Missing error for request with incorrect url. Url:%s", incorrectUrlSite.Url)
	}
}

func TestInvalidRssParsing(t *testing.T) {
	site := Site{
		Url:                 "http://ya.ru/",
		NewsItemBlock:       ".wrong_selector",
		NewsItemTitle:       ".wrong_selector",
		NewsItemDescription: ".wrong_selector",
		NewsItemLink:        ".wrong_selector",
		NewsItemDate:        ".wrong_selector",
	}

	resp, err := http.Get(site.Url)
	if err != nil {
		t.Errorf("Can't execute a get request to url:%s", site.Url)
	}
	defer resp.Body.Close()

	_, err = parseRss(resp, site)
	if err == nil {
		t.Errorf("Missing error from invalid source for rss parsing. Url:%s", site.Url)
	}
}

func TestInvalidHtmlParsing(t *testing.T) {
	site := Site{
		Url:                 "https://lenta.ru/rss/news",
		NewsItemBlock:       ".wrong_selector",
		NewsItemTitle:       ".wrong_selector",
		NewsItemDescription: ".wrong_selector",
		NewsItemLink:        ".wrong_selector",
		NewsItemDate:        ".wrong_selector",
	}

	resp, err := http.Get(site.Url)
	if err != nil {
		t.Errorf("Can't execute a get request to url:%s", site.Url)
	}
	defer resp.Body.Close()

	_, err = parseHtml(resp, site)
	if err == nil {
		t.Errorf("Missing error from invalid source for html parsing. Url:%s", site.Url)
	}
}

func TestRssSuccessfullParsing(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, _ = w.Write([]byte(
			`<?xml version="1.0" encoding="UTF-8"?>
				<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
					<channel>
					<item>
						<title>Заголовок 1</title>
						<link>https://rss_news/1</link>
						<description>Описание 1</description>
						<pubDate>Thu, 28 Apr 2022 13:36:00 +0300</pubDate>
				  	</item>
				  	<item>
					  <title>Заголовок 2</title>
					  <link>https://rss_news/2</link>
					  <description>Описание 2</description>
					  <pubDate>Thu, 28 Apr 2022 15:30:00 +0300</pubDate>
				  	</item>
					</channel>
				</rss>
			`))
	}))
	defer server.Close()

	site := Site{Url: server.URL}
	news, err := testParser.Parse(site)
	if err != nil {
		t.Errorf("Error while parse correct rss source: %s", err)
	}

	expectedNewsItem1 := NewsItem{
		Title:       "Заголовок 1",
		Description: "Описание 1",
		Link:        "https://rss_news/1",
		Date:        "Thu, 28 Apr 2022 13:36:00 +0300",
	}

	expectedNewsItem2 := NewsItem{
		Title:       "Заголовок 2",
		Description: "Описание 2",
		Link:        "https://rss_news/2",
		Date:        "Thu, 28 Apr 2022 15:30:00 +0300",
	}

	expectedResult := []NewsItem{}
	expectedResult = append(expectedResult, expectedNewsItem1, expectedNewsItem2)

	for i := range news {
		if news[i] != expectedResult[i] {
			t.Errorf("Unexprected result from the correct rss source: %s", err)
		}
	}
}

func TestHtmlSuccessfulParsing(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, _ = w.Write([]byte(`<!DOCTYPE html>
			<html>
				<body>
					<article class="art">
						<a href="https://html_news.ru/1"><h2>Заголовок 1</h2></a>
						<div class="art-desc">Описание 1</div>
						<i>Wed, 25 Sep 2019 18:10:34 +0300</i>
					</article>
					<article class="art">
						<a href="https://html_news.ru/2"><h2>Заголовок 2</h2></a>
						<div class="art-desc">Описание 2</div>
					</article>
				</body>
			</html>
		`))
	}))
	defer server.Close()

	site := Site{
		Url:                 server.URL,
		NewsItemBlock:       "article",
		NewsItemTitle:       "h2",
		NewsItemDescription: ".art-desc",
		NewsItemLink:        "a",
		NewsItemDate:        "i",
	}

	news, err := testParser.Parse(site)
	if err != nil {
		t.Errorf("Error while parse correct html source: %s", err)
	}

	expectedNewsItem1 := NewsItem{
		Title:       "Заголовок 1",
		Description: "Описание 1",
		Link:        "https://html_news.ru/1",
		Date:        "Wed, 25 Sep 2019 18:10:34 +0300",
	}

	expectedNewsItem2 := NewsItem{
		Title:       "Заголовок 2",
		Description: "Описание 2",
		Link:        "https://html_news.ru/2",
	}

	expectedResult := []NewsItem{}
	expectedResult = append(expectedResult, expectedNewsItem1, expectedNewsItem2)

	for i := range news {
		if news[i] != expectedResult[i] {
			t.Errorf("Unexprected result from the correct html source: %s", err)
		}
	}
}
