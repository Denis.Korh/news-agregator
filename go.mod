module news-agregator

go 1.16

require (
	github.com/PuerkitoBio/goquery v1.8.0
	github.com/gorilla/mux v1.8.0
	github.com/lib/pq v1.10.5
	github.com/stretchr/testify v1.7.1
)
