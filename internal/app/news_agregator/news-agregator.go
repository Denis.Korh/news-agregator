package news_agregator

import (
	"fmt"
	"html/template"
	"io"
	"log"
	"net/http"
	"sync"

	"news-agregator/internal/pkg/parsing"

	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
)

var templates = []string{
	"web/templates/index.html",
	"web/templates/addSourcePage.html",
	"web/templates/sourcesList.html",
}

type Model interface {
	GetSites() (siteItems []parsing.Site, err error)
	DeleteAllSites() (err error)
	AddNewsItem(news_item parsing.NewsItem) (err error)
	AddSite(site parsing.Site) (err error)
	DeleteSite(siteId string) (err error)
	NewsSearch(searchString string) (allNews []parsing.NewsItem, err error)
}

type Parser interface {
	Parse(siteItem parsing.Site) (news []parsing.NewsItem, err error)
}

type Templates interface {
	ExecuteTemplate(wr io.Writer, name string, data interface{}) error
}

type news_agregator struct {
	db        Model
	parser    Parser
	templates Templates
}

func NewApplication(db Model, parser Parser, templates Templates) *news_agregator {
	return &news_agregator{
		db:        db,
		parser:    parser,
		templates: templates,
	}
}

func PrepareTemplates() (t *template.Template) {
	t, err := template.ParseFiles(templates...)
	if err != nil {
		log.Printf("Error during template parsing, err: %s", err)
	}

	return
}

func (na *news_agregator) index(w http.ResponseWriter, r *http.Request) {
	siteItems, err := na.db.GetSites()
	if err != nil {
		log.Printf("Can't get sites from DB, err: %s", err)
	}

	allNews := []parsing.NewsItem{}
	mu := &sync.Mutex{}
	wg := sync.WaitGroup{}
	for _, siteItem := range siteItems {
		wg.Add(1)
		go func(siteItem parsing.Site, wg *sync.WaitGroup) {
			news, err := na.parser.Parse(siteItem)
			if err != nil {
				log.Printf("Parsing error from url %s :\n%s", siteItem.Url, err)
			}
			mu.Lock()
			allNews = append(allNews, news...)
			mu.Unlock()

			defer wg.Done()
		}(siteItem, &wg)
	}
	wg.Wait()

	err = na.db.DeleteAllSites()
	if err != nil {
		log.Print("Can't truncate table sites")
	}

	for _, news_item := range allNews {
		err = na.db.AddNewsItem(news_item)
		if err != nil {
			log.Printf("Insert error: %s", err)
		}
	}

	na.templates.ExecuteTemplate(w, "index", allNews)
}

func (na *news_agregator) addSource(w http.ResponseWriter, r *http.Request) {
	na.templates.ExecuteTemplate(w, "addSourcePage", nil)
}

func (na *news_agregator) newSource(w http.ResponseWriter, r *http.Request) {
	siteItem := parsing.Site{
		Url:                 r.FormValue("url"),
		NewsItemBlock:       r.FormValue("news_item_block"),
		NewsItemTitle:       r.FormValue("news_item_title"),
		NewsItemDescription: r.FormValue("news_item_description"),
		NewsItemLink:        r.FormValue("news_item_link"),
		NewsItemDate:        r.FormValue("news_item_date"),
	}

	err := na.db.AddSite(siteItem)
	if err != nil {
		log.Printf("Can't add news site to DB, err: %s", err)
	}

	http.Redirect(w, r, "/sources_list", http.StatusSeeOther)
}

func (na *news_agregator) sourcesList(w http.ResponseWriter, r *http.Request) {
	siteItems, err := na.db.GetSites()
	if err != nil {
		log.Printf("Can't get sites from DB, err: %s", err)
	}

	na.templates.ExecuteTemplate(w, "sourcesList", siteItems)
}

func (na *news_agregator) deleteSource(w http.ResponseWriter, r *http.Request) {
	siteId := r.FormValue("Id")

	err := na.db.DeleteSite(siteId)
	if err != nil {
		log.Printf("Can't delete site from DB, err: %s", err)
	}

	http.Redirect(w, r, "/sources_list", http.StatusSeeOther)
}

func (na *news_agregator) search(w http.ResponseWriter, r *http.Request) {
	allNews, err := na.db.NewsSearch(r.FormValue("searchString"))
	if err != nil {
		log.Printf("Can't find news in DB, err: %s", err)
	}

	na.templates.ExecuteTemplate(w, "index", allNews)
}

func (na *news_agregator) Start(port string) {
	r := mux.NewRouter()
	r.HandleFunc("/", na.index).Methods("GET")
	r.HandleFunc("/add_source_page", na.addSource).Methods("GET")
	r.HandleFunc("/new_source", na.newSource).Methods("POST")
	r.HandleFunc("/sources_list", na.sourcesList).Methods("GET")
	r.HandleFunc("/deleteSource", na.deleteSource).Methods("POST")
	r.HandleFunc("/search", na.search).Methods("POST")
	r.Handle("/", r)

	fmt.Println("Server started at " + port)
	http.ListenAndServe(port, r)
}
