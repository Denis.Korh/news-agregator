package main

import (
	"database/sql"
	"log"
	"net/http"
	"news-agregator/internal/app/news_agregator"
	"news-agregator/internal/pkg/model"
	"news-agregator/internal/pkg/parsing"
)

func main() {
	db, err := sql.Open("postgres", "host=localhost port=5432 user=postgres password=postgres dbname=newsagregator sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	app := news_agregator.NewApplication(
		model.NewModel(db),
		parsing.NewParser(http.Client{}),
		news_agregator.PrepareTemplates(),
	)

	app.Start(":8080")
}